# Running the W+D comparison on perlmutter

From the base directory run the image setup script:

```
source perlmutter-image-setup.sh
```

Navigate into the Rivet routines folder and build the routine:

```
cd rivet-routines/STDM-2019-22_Wc
source build.sh
```

Run the local test Rivet job:

```
athena local_jo.py
```

This will process one Sherpa2.2.11 W+jets (c-slice) EVNT file and create the following output: `Sh_2211_Wenu_maxHTpTV2_CFilterBVeto.700339.yoda.gz`.

## Creating plots

The `plot.sh` script can be used to create plots from the `Sh_2211_Wenu_maxHTpTV2_CFilterBVeto.700339.yoda.gz` output file. A new terminal window needs to be opened because the image from the previous step does not have the `convert` package to convert PNG files into PDF files. From the base directory run the image setup script:

```
source setup-no-image.sh
```

Navigate into the Rivet routines folder and run the script:

```
cd rivet-routines/STDM-2019-22_Wc
source plot.sh
```

This should create an output folder called `plots` with the results in `plots/ATLAS_2023_I2628732`.
