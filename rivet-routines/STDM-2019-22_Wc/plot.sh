#!/bin/bash

export RIVET_ANALYSIS_PATH=$PWD
OUTFILE="Sh_2211_Wenu_maxHTpTV2_CFilterBVeto.700339.yoda.gz"

# plot
rivet-mkhtml --errs -o plots $OUTFILE:"Title=Sherpa2.2.11"
